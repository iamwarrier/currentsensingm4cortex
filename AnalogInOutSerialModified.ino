/*
  Analog input, analog output, serial output
 
 Reads an analog input pin, maps the result to a range from 0 to 255
 and uses the result to set the pulsewidth modulation (PWM) of an output pin.
 Also prints the results to the serial monitor.
 
 The circuit:
 * potentiometer connected to analog pin 0.
   Center pin of the potentiometer goes to the analog pin.
   side pins of the potentiometer go to +3.3V and ground
 * LED connected from digital pin 9 to ground
 
 created 29 Dec. 2008
 modified 30 Aug 2011
 by Tom Igoe
 
 This example code is in the public domain.
 
 */

// These constants won't change.  They're used to give names
// to the pins used:
const int analogInPin = A0;  // Analog input pin that the potentiometer is attached to
const int analogOutPin = 9; // Analog output pin that the LED is attached to

int sensorValue = 0;        // value read from the pot
int outputValue = 0;        // value output to the PWM (analog out)

double voltage = 0 ;
double current  = 0 ;
double Sum=0;
double sensorValuet = 0 ; 
double sensorValuet_initial = 0 ; 
int calibration = 1 ;
double current_measured[64];
int index1 = -1 ;
uint8_t sample_count = 0;
double avg_current_measured = 0 ;
int avg2 = 1;
void setup() {
  // initialize serial communications at 9600 bps:
  Serial.begin(9600); 
}

void loop() {
  
  // read the analog in value:
   while(calibration)
   {
     for(int i = 0 ; i<1000;i++)
  {
     sensorValue = analogRead(analogInPin);    
     Sum = Sum + sensorValue ;  
    //  delay(10);     
  }
  sensorValuet_initial = Sum/1000;
  Sum = 0 ;
 

    calibration = 0 ;
    
   }
  for(int i = 0 ; i<10000;i++)
  {
      
     sensorValue = analogRead(analogInPin);    
     Sum = Sum + sensorValue ; 
     // delay(10);      
  }
  sensorValuet = Sum/10000;
  Sum = 0 ;
 int i=0 ; 

  
  // map it to the range of the analog out:
 //  sensorValue = analogRead(analogInPin);    
 // outputValue = map(sensorValue, 0, 1023, 0, 255);  
  // change the analog out value:
  analogWrite(analogOutPin, outputValue);           

  //0.0803
  // voltage =  ((sensorValue) * (62.5 * 0.323));
   voltage = ((abs((sensorValuet)-sensorValuet_initial)*803)) ; 
   current = (voltage * 0.00015);
  // print the results to the serial monitor:

 /* while(avg2)
  {
  current_measured[index++] = current ; 
   if (index >= 64)
            {
                avg2 = 0;
            }
  }*/

 index1 = ((index1 + 1) % 5);
 current_measured[index1] =  current ; 
   if (sample_count < 5)
                    {
                        sample_count++;
                    }
   

 double mean = 0;
    uint8_t n = 1;
  
    for (uint8_t i = 0; i < sample_count; i++)
    {
        mean += ((current_measured[i] - mean) / n);
        n++;
    }

     avg_current_measured = mean ; 



 Serial.print("\n " );                       
  Serial.print(avg_current_measured);      
 // Serial.print("\t output = ");      
 // Serial.println(current);   
  // wait 10 milliseconds before the next loop
  // for the analog-to-digital converter to settle
  // after the last reading:
                
}
